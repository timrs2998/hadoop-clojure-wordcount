## Hadoop Wordcount in Clojure ##
The [standard wordcount example](https://wiki.apache.org/hadoop/WordCount) for Hadoop 1.0.3 written in Clojure as a leiningen project. The Clojure code is adapted from the [clojure-hadoop](https://github.com/stuartsierra/clojure-hadoop) library.

### Dependencies ###

Compiling and running this project required git, hadoop, the JDK, and leiningen. To acquire everything except hadoop, you can try the following:
    
    sudo apt-get install git leiningen default-jdk

If you have lein 2.x or greater, you should be able to follow the instructions. Otherwise uncomment the hadoop-core dependency in project.clj.

If you want to download Hadoop 1.0.3 and temporarily add it to the path, then you can do:

    # Acquiring and uncompressing Hadoop
    wget https://archive.apache.org/dist/hadoop/core/hadoop-1.0.3/hadoop-1.0.3.tar.gz
    tar -zxvf hadoop-1.0.3.tar.gz

    # Setting environment variables
    export HADOOP_HOME=$(pwd)/hadoop-1.0.3
    export HADOOP_VERSION=1.0.3
    export PATH=$PATH:$HADOOP_HOME/bin/

Don't close the terminal or the environment variables will be lost.

### Compiling ###

You can acquire the source from either [GitHub](https://github.com/timrs2998/hadoop-clojure-wordcount) or [Bitbucket](https://bitbucket.org/timrs2998/hadoop-clojure-wordcount) and compile it as a single jar using leiningen like so:

    git clone https://bitbucket.org/timrs2998/hadoop-clojure-wordcount.git
    cd hadoop-clojure-wordcount/
    lein uberjar
    # Now target/ contains a fat jar

### Running ###

With hadoop in the path, you can run the project on text from [Project Gutenberg](http://www.gutenberg.org/) like so:

    wget http://www.gutenberg.org/cache/epub/1342/pg1342.txt
    hadoop jar target/wordcount-0.1.0-SNAPSHOT-standalone pg1342.txt output/

The output/part-r-00000 file will now contain the counts of each word in Pride and Prejudice. To see the output you can run:
    
    more output/part-r-00000

### Licensing ###

All code is licensed under the GNU GENERAL PUBLIC LICENSE Version 3.
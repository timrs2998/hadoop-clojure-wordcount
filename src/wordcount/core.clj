(ns wordcount.core
  (:import (java.util StringTokenizer)
           (org.apache.hadoop.util Tool GenericOptionsParser)
           (org.apache.hadoop.io IntWritable Text LongWritable)
           (org.apache.hadoop.fs Path)
           (org.apache.hadoop.mapreduce Job Mapper Reducer)
           (org.apache.hadoop.mapreduce.lib.input FileInputFormat)
           (org.apache.hadoop.mapreduce.lib.output FileOutputFormat)
           (org.apache.hadoop.mapred OutputCollector))
  (:gen-class :main true))

;; Generate Mapper and Reducer classes
(gen-class
  :name "wordcount.core_mapper"
  :extends "org.apache.hadoop.mapreduce.Mapper"
  :prefix "mapper-")
(gen-class
  :name "wordcount.core_reducer"
  :extends "org.apache.hadoop.mapreduce.Reducer"
  :prefix "reducer-")

(defn mapper-map
  "This is our implementation of the Mapper.map method.  The key and
  value arguments are sub-classes of Hadoop's Writable interface, so
  we have to convert them to strings or some other type before we can
  use them.  Likewise, we have to call the context.write method with 
  them."
  [this key value context]
  (doseq [word (enumeration-seq (StringTokenizer. (str value)))]
    (.write context (Text. word) (LongWritable. 1))))

(defn reducer-reduce
  "This is our implementation of the Reducer.reduce method.  The key
  argument is a sub-class of Hadoop's Writable, while 'values' is a Java
  Iteratorable that Clojure treats as a sequence.  

  When you get an object from 'values', you must extract its value 
  (as we do here with the 'get' method) immediately, before accepting 
  the next value from the iterator.  That is, you cannot hang on to past 
  values."
  [this key values context]
  (let [sum (reduce + (map (fn [#^LongWritable v] (.get v)) values))]
    (.write context key (LongWritable. sum))))

(defn -main
  "Main method creates and submits a job for completion."
  [& args]
  (let [conf (org.apache.hadoop.conf.Configuration.)]
    (let [otherArgs (.getRemainingArgs
          (org.apache.hadoop.util.GenericOptionsParser. conf (into-array String args)))]
      ; Ensure there are 2 cmd-line arguments
      (if (not= (count otherArgs) 2)
        ((.println (System/err) "Usage: wordcount <in> <out>")
          (System/exit 2)))

      ; Create a new job
      (let [job (doto (Job. conf "Clojure WordCount")
                  (.setJarByClass (Class/forName "wordcount.core"))
                  (.setMapperClass (Class/forName "wordcount.core_mapper"))
                  (.setReducerClass (Class/forName "wordcount.core_reducer"))
                  (.setOutputKeyClass Text)
                  (.setOutputValueClass LongWritable))]

        (FileInputFormat/addInputPath job (Path. (first otherArgs)))
        (FileOutputFormat/setOutputPath job (Path. (second otherArgs)))

        ; Submit job for completion
        (System/exit
          (if (.waitForCompletion job true) 1 0))))) 0)
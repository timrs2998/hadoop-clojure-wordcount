(defproject wordcount "0.1.0-SNAPSHOT"
  :license {:name "GNU GENERAL PUBLIC LICENSE, Version 3"
            :url "http://www.gnu.org/licenses/gpl-3.0.en.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 ; Uncomment the following line for lein < 2.x:
                 ; [org.apache.hadoop/hadoop-core "1.0.3"]
                 ]
  :profiles {:provided
             {:dependencies
              [[org.apache.hadoop/hadoop-core "1.0.3"]]}}
  :main wordcount.core
  :aot [wordcount.core])
